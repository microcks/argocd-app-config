#### Commands

```bash

oc policy add-role-to-user \
    system:image-puller system:serviceaccount:dev:default \
    --namespace=argo

oc policy add-role-to-group \
    system:image-puller system:serviceaccounts:dev \
    --namespace=argo




crear nuevo namespace y ser administrado por otro namespace

apiVersion: v1
kind: Namespace
metadata:
#cambio
  name: qa
  labels:
    argocd.argoproj.io/managed-by: argo


```
</br>

#### Links

* Config repo: [https://gitlab.com/nanuchi/argocd-app-config](https://gitlab.com/nanuchi/argocd-app-config)

* Docker repo: [https://hub.docker.com/repository/docker/nanajanashia/argocd-app](https://hub.docker.com/repository/docker/nanajanashia/argocd-app)

* Install ArgoCD: [https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd](https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd)

* Login to ArgoCD: [https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli](https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli)

* ArgoCD Configuration: [https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)
